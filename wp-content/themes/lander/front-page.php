<?php
/**
 * The custom template for the one-page style front page. Kicks in automatically.
 */

get_header(); ?>
<?php
global $more;
?>
	<div id="primary" class="content-area lander-page">
		<main id="main" class="site-main" role="main">


					<section id="futuretoday">
						<div class="container">
							<div class="row">

								<div class="col-md-6 text-center first padding_none">
									<?php 
									$query = new WP_Query( 'pagename=budushhee-segodnya' );
									// The Loop
									if ( $query->have_posts() ) {
										while ( $query->have_posts() ) {
											$query->the_post();
											the_content();
										}
									}								
									/* Restore original Post Data */
									wp_reset_postdata();
									?>
								</div><!-- col-md-6 first -->



								<div class="col-md-6 padding_right second">
									<div class="row margin_none">
										<div class="col-md-5 padding_none">
											<?php 
												$query = new WP_Query( 'pagename=budushhee-segodnya-tsena' );
												// The Loop
												if ( $query->have_posts() ) {
													while ( $query->have_posts() ) {
														$query->the_post();
														the_content();
													}
												}								
												/* Restore original Post Data */
												wp_reset_postdata();
											?>
											<?php 
												$query = new WP_Query( 'pagename=video' );
												// The Loop
												if ( $query->have_posts() ) {
													while ( $query->have_posts() ) {
														$query->the_post();
														the_content();
													}
												}								
												/* Restore original Post Data */
												wp_reset_postdata();
											?>
										</div><!-- col-md-5 -->
										<div class="col-md-6 col-md-offset-1 padding_none">
											<?php 
												$query = new WP_Query( 'pagename=forma' );
												// The Loop
												if ( $query->have_posts() ) {
													while ( $query->have_posts() ) {
														$query->the_post();
														the_content();
													}
												}								
												/* Restore original Post Data */
												wp_reset_postdata();
											?>
										</div><!-- col-md-6 -->
										<div class="col-md-12 padding_none">
											<?php 
												$query = new WP_Query( 'pagename=budushhee-segodnya-tekst' );
												// The Loop
												if ( $query->have_posts() ) {
													while ( $query->have_posts() ) {
														$query->the_post();
														the_content();
													}
												}								
												/* Restore original Post Data */
												wp_reset_postdata();
											?>
										</div><!-- col-md-12 -->
									</div><!-- row -->
								</div><!-- col-md-6 first -->



								<!-- <div class="col-md-6 padding_right second">
									<div class="row margin_none">
										<div class="col-md-5 padding_none">
											<p>Специальная цена<br> Только до <span id="tilltime">01.02.2016</span> всего</p>
											<div class="label_price vert_middle">
												<div>
													<p class="new_price">690 руб.</p>
													<p class="old_price">вместо <span class="throw_lined">1190 руб.</span></p>
												</div>
											</div>
											<div class="video"><a class="fancybox link" title="youtube" href="https://www.youtube.com/embed/jdkCXHZNBgs?autoplay=1" data-type="iframe"><img class="alignnone size-medium wp-image-51" src="http://localhost/wp_samsung/wp-content/uploads/2016/01/samsung-360-sound-300x144.jpg" alt="samsung-360-sound" width="300" height="144" /></a></div>
										</div>
										<div class="col-md-6 col-md-offset-1 padding_none form_block">
											<div class="row margin_none">
												<div class="countdown">
													<div class="col-md-3 text-center padding_none">
														<div class="days">40</div>
														<em class="textDays">Дней</em>
													</div>
													<div class="col-md-3 text-center padding_none">
														<div class="hours">12</div>
														<em class="textHours">Часов</em>
													</div>
													<div class="col-md-3 text-center padding_none">
														<div class="minutes">27</div>
														<em class="textMinutes">Минут</em>
													</div>
													<div class="col-md-3 text-center padding_none">
														<div class="seconds">20</div>
														<em class="textSeconds">Секунд</em>
													</div>
												</div>
											</div>
											<div class="row text-center superprice">
												<p>Заказать по<br> суперцене</p>
											</div>
											<div class="row last">
												<form id="contact_form" class="form" action="localhost/wp_samsung/wp-content/themes/lander/js/contact.php" novalidate="novalidate" data-selector="form">
												<div class="form-group padding_left">
													<input id="quantity" class="form-control" name="quantity" type="text" placeholder="1шт." /><span class="minus" onclick="minus($('#quantity'))"></span><span class="plus" onclick="plus($('#quantity'))"></span>
												</div>
												<div class="form-group padding_left phone">
													<input id="contact_phone" class="form-control" name="phone" type="text" placeholder="Телефон" />
												</div>
												<div><button id="contact_submit" class="btn btn-default btn-block" type="submit">Заказать</button></div>
												</form>
											</div>
										</div>
										<div class="col-md-12 padding_right">
											<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum tempus euismod enim eu pulvinar. In hac habitasse platea dictumst. Phasellus sit amet lectus eget sapien condimentum ultrices nec ac mauris. In volutpat libero eros, hendrerit maximus dolor placerat eu.</p>
										</div>
									</div>
								</div> -->

						</div><!-- row -->
						</div><!-- container -->
					</section><!-- #futuretoday -->

					<section id="testimonials">
						<div class="container">
							<div class="row">
								<?php 
								$args = array(
									'posts_per_page' => 3,
									'orderby' => 'rand',
									'category_name' => 'testimonials'
								);
															
								$query = new WP_Query( $args );
								// The Loop
									echo '<div class="col-md-6">';
								if ( $query->have_posts() ) {
									while ( $query->have_posts() ) {
										$query->the_post();
										$more = 0;
										echo '<div class="row item_block">';
										echo '<div class="col-md-4 padding_left">';
										echo '<img class="img img-responsive" src='.the_post_thumbnail('testimonial-mug').'>';
										echo '<h3 class="testimonial-name">' . get_the_title() . '</h3>';
										echo '<p class="text-center position">'.get_post_meta($post->ID,'должность',true).'</p>';
										echo '</div>';
										echo '<div class="col-md-8 padding_right">';
										echo '<div class="testimonial-excerpt">';
										the_content('');
										echo '</div>';
										echo '</div>';
										echo '</div>';
									}
								}
									echo '</div>';

								/* Restore original Post Data */
								wp_reset_postdata();
								?>



								<div class="col-md-6 padding_right second">
									<div class="row margin_none">
										<div class="col-md-5 padding_none">
											<?php 
												$query = new WP_Query( 'pagename=budushhee-segodnya-tsena' );
												// The Loop
												if ( $query->have_posts() ) {
													while ( $query->have_posts() ) {
														$query->the_post();
														the_content();
													}
												}								
												/* Restore original Post Data */
												wp_reset_postdata();
											?>
										</div><!-- col-md-5 -->
										<div class="col-md-6 col-md-offset-1 padding_none">
											<?php 
												$query = new WP_Query( 'pagename=forma2' );
												// The Loop
												if ( $query->have_posts() ) {
													while ( $query->have_posts() ) {
														$query->the_post();
														the_content();
													}
												}								
												/* Restore original Post Data */
												wp_reset_postdata();
											?>
										</div><!-- col-md-6 -->
									</div><!-- row -->
								</div><!-- col-md-6 first -->


							</div><!-- row -->
						</div><!-- container -->
					</section><!-- #testimonials -->

					<section id="whywe">
						<div class="container">
							<div class="row">
								<?php 
								$query = new WP_Query( 'pagename=pochemu-imenno-my' );
								$whywe_id = $query->queried_object->ID;

								// The Loop
								if ( $query->have_posts() ) {
									while ( $query->have_posts() ) {
										$query->the_post();
										$more = 0;									
										echo '<h2 class="section-title text-center">' . get_the_title() . '</h2>';
										echo '<div class="entry-content col-md-12">';
										the_content('');
										echo '</div>';
									}
								}

								/* Restore original Post Data */
								wp_reset_postdata();

								$args = array(
									'post_type' => 'page',
									'orderby' => 'random',
									'post_parent' => $whywe_id
								);
								$whywe_query = new WP_Query( $args );
								
								echo '<div class="row">';	
								// The Loop
								if ( $whywe_query->have_posts() ) {
																
									while ( $whywe_query->have_posts() ) {
										$whywe_query->the_post();
										$more = 0;
										echo '<div class="col-md-3 text-center">';
											the_post_thumbnail('testimonial-mug');
											the_content('');
										echo '</div>';
									}
								}

								/* Restore original Post Data */
								wp_reset_postdata();
								echo '</div>';
								?>
							</div><!-- row -->
						</div><!-- container -->
					</section><!-- whywe -->

					<section id="thousands">
						<div class="container">
							<div class="row">
							<?php 
							$query = new WP_Query( 'pagename=pochemu-tyisyachi-pokupateley-vyibrali-nas' );
							// The Loop
							if ( $query->have_posts() ) {
								while ( $query->have_posts() ) {
									$query->the_post();
									echo '<h2 class="section-title text-center">' . get_the_title() . '</h2>';
									echo '<div class="col-md-3">';
									the_post_thumbnail('');
									echo '</div>';
									echo '<div class="entry-content col-md-4">';
									the_content();
									echo '</div>';
								}
							}

							/* Restore original Post Data */
							wp_reset_postdata();
							?>

								<div class="col-md-5 padding_right second">
									<div class="row margin_none">
										<div class="col-md-5 padding_none">
											<p>Специальная цена<br> Только до <span id="tilltime">01.02.2016</span> всего</p>
											<div class="label_price vert_middle">
												<div>
													<p class="new_price">690 руб.</p>
													<p class="old_price">вместо <span class="throw_lined">1190 руб.</span></p>
												</div>
											</div><!-- label_price -->
										</div><!-- col-md-5 -->
										<div class="col-md-6 col-md-offset-1 padding_none form_block">
											<div class="row margin_none">
												<div class="countdown">
													<div class="col-md-3 text-center padding_none">
														<div class="days">40</div>
														<em class="textDays">Дней</em>
													</div>
													<div class="col-md-3 text-center padding_none">
														<div class="hours">12</div>
														<em class="textHours">Часов</em>
													</div>
													<div class="col-md-3 text-center padding_none">
														<div class="minutes">27</div>
														<em class="textMinutes">Минут</em>
													</div>
													<div class="col-md-3 text-center padding_none">
														<div class="seconds">20</div>
														<em class="textSeconds">Секунд</em>
													</div>
												</div><!-- countdown -->
											</div><!-- row -->
											<div class="row text-center superprice">
												<p>Заказать по<br> суперцене</p>
											</div><!-- row -->
											<div class="row last">
												<form id="contact_form3" class="form" action="localhost/wp_samsung/wp-content/themes/lander/js/contact.php" novalidate="novalidate" data-selector="form">
												<div class="form-group padding_left">
													<input id="quantity3" class="form-control" name="quantity" type="text" placeholder="1шт." /><span class="minus" onclick="minus($('#quantity'))"> </span><span class="plus" onclick="plus($('#quantity'))"> </span>
												</div>
												<div class="form-group padding_left phone">
													<input id="contact_phone3" class="form-control" name="phone" type="text" placeholder="Телефон" />
												</div>
												<div><button id="contact_submit3" class="btn btn-default btn-block" type="submit">Заказать</button></div>
												</form>
											</div>
										</div><!-- col-md-6 -->
									</div><!-- row -->
								</div><!-- col-md-6 second -->


						</div><!-- row -->
						</div><!-- container -->
					</section><!-- #meet -->

					<!-- <section id="contact">
						<div class="container">
							<div class="row">
								<?php 
								$query = new WP_Query( 'pagename=contact' );
								// The Loop
								if ( $query->have_posts() ) {
									while ( $query->have_posts() ) {
										$query->the_post();
										echo '<h2 class="section-title">' . get_the_title() . '</h2>';
										echo '<div class="entry-content">';
										the_content();
										echo '</div>';
									}
								}

								
								wp_reset_postdata();
								?>
							</div>
						</div>
					</section> -->

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>
