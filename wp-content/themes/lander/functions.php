<?php
/*
	The functions file for the lander child theme
*/

function lander_scripts(){
	if( is_front_page()){
		// wp_enqueue_style('lander-styles', get_stylesheet_directory_uri().'/lander-styles.css');

		wp_enqueue_style('lander-bootstrap', get_stylesheet_directory_uri()."/css/bootstrap.css");
	    wp_enqueue_style('lander-styles', get_stylesheet_directory_uri()."/css/jquery.fancybox.css");
	    // wp_enqueue_style('styles', get_stylesheet_directory_uri()."/css/styles.css");
	    wp_enqueue_style('lander-magnific', get_stylesheet_directory_uri()."/css/magnific-popup.css"); 
	    wp_enqueue_style('lander-iconfont', get_stylesheet_directory_uri()."/css/iconfont-style.css");
	    wp_enqueue_style('lander-animate', get_stylesheet_directory_uri()."/js/animations/animate.css");
	    wp_enqueue_style('lander-styles', get_stylesheet_directory_uri()."/css/bootstrap-datepicker3.min.css");

		wp_enqueue_script('lander-script', get_stylesheet_directory_uri().'/js/jquery-1.11.2.min.js', array('jquery'));
		wp_enqueue_script('lander-script', get_stylesheet_directory_uri().'/js/landerscripts.js', array('jquery'));

	    wp_enqueue_script('lander-datepicker', get_stylesheet_directory_uri()."/js/fancybox.pack.js", array('jquery'));
		wp_enqueue_script('lander-bootstrap', get_stylesheet_directory_uri()."/js/bootstrap.min.js", array('jquery')); 
		wp_enqueue_script('lander-validate', get_stylesheet_directory_uri()."/js/jquery.validate.min.js", array('jquery'));
		wp_enqueue_script('lander-smoothscroll', get_stylesheet_directory_uri()."/js/smoothscroll.js", array('jquery')); 
		wp_enqueue_script('lander-smooth-scroll', get_stylesheet_directory_uri()."/js/jquery.smooth-scroll.min.js", array('jquery')); 
		wp_enqueue_script('lander-placeholders', get_stylesheet_directory_uri()."/js/placeholders.jquery.min.js", array('jquery')); 
		wp_enqueue_script('lander-magnific-popup', get_stylesheet_directory_uri()."/js/jquery.magnific-popup.min.js", array('jquery'));
		wp_enqueue_script('lander-counterup', get_stylesheet_directory_uri()."/js/jquery.counterup.min.js", array('jquery'));
		wp_enqueue_script('lander-waypoints', get_stylesheet_directory_uri()."/js/waypoints.min.js", array('jquery'));
		wp_enqueue_script('lander-video', get_stylesheet_directory_uri()."/js/video.js", array('jquery'));
		wp_enqueue_script('lander-bigvideo', get_stylesheet_directory_uri()."/js/bigvideo.js", array('jquery'));
	    wp_enqueue_script('lander-wow', get_stylesheet_directory_uri()."/js/animations/wow.min.js", array('jquery'));
	    wp_enqueue_script('lander-jCounter', get_stylesheet_directory_uri()."/js/jquery.jCounter-0.1.4.js", array('jquery'));
	    wp_enqueue_script('lander-datepicker', get_stylesheet_directory_uri()."/js/bootstrap-datepicker.min.js", array('jquery'));
		wp_enqueue_script('lander-custom', get_stylesheet_directory_uri()."/js/custom.js", array('jquery'));
	}
}

add_action('wp_enqueue_scripts','lander_scripts');

add_image_size('testimonial-mug', 253, 253, true);

function exclude_testimonials( $query ){
	if( !$query->is_category('testimonials') && $query->is_main_query()){
		$query->set('cat','-5');
	}
}
add_action('pre_get_posts', 'exclude_testimonials');